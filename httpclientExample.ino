// Author : Sanjeev Kumar
// Contact: sanjeev at outlook Dot in
#include <JsonParserGeneratorRK.h>
#include "HttpClient.h"

JsonParserStatic<512, 50> jsonParser;
String ip;
unsigned int nextTime = 0;    // Next time to contact the server
HttpClient http;

http_header_t headers[] = {
    { "Authorization", "Basic QWxhZGRpbjpPcGVuU2VzYW1l" },
    { "Accept" , "*/*"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};

http_request_t request;
http_response_t response;

void setup() {
    Serial.begin(9600);
}

void loop() {
    if (nextTime > millis()) {
        return;
    }
    

    request.hostname = "192.168.5.17";
    request.port = 80;
    request.path = "/flaskapp/tcpserver";
    if (ip == NULL ) {
        http.get(request, response, headers);
        Particle.publish("Response Status: ", String::format("%3i",response.status));
        Particle.publish("Request body: ", response.body);
        jsonParser.addString(response.body);
        if (jsonParser.parse()) {
            jsonParser.getOuterValueByKey("tcpserver", ip);
            Particle.publish("Got TCPServer", ip, PRIVATE);
        }
    } else {
         Particle.publish("Already had TCPServer", ip, PRIVATE);
    }



    nextTime = millis() + 10000;
}